const btnReadMoreMessage = document.querySelector(".message__block-text-button")
btnReadMoreMessage.addEventListener("click", () => {
  document.querySelector(".message__block-text-content").classList.add("text")
  document.querySelector(".message__block-text-paragraph").classList.remove("invisible")
  btnReadMoreMessage.classList.toggle("invisible")
})