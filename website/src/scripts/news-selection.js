const btnArrowNext = document.querySelector(".news__menu-arrow-next")
const btnArrowPrev = document.querySelector(".news__menu-arrow-prev")

const menuItems = [...document.querySelectorAll(".btns-menu")]

// const items = document.querySelector('.news__menu-list')

btnArrowNext.addEventListener("click", () => {
  if (menuItems[6].classList.contains("invisible")) {
    for (let i = 0; i < 6; i++) {
      menuItems[i].classList.add("invisible")
    }
    for (let i = 6; i < 12; i++) {
      menuItems[i].classList.remove("invisible")
    }
    btnArrowPrev.classList.remove("invisible")
  } else if (menuItems[0].classList.contains("invisible") && menuItems[14].classList.contains("invisible")) {
    for (let i = 12; i < menuItems.length; i++) {
      menuItems[i].classList.remove("invisible")
    }
    for (let i = 6; i < 9; i++) {
      menuItems[i].classList.add("invisible")
    }
    btnArrowNext.classList.add("invisible")
  }
})

btnArrowPrev.addEventListener("click", () => {
  if (menuItems[0].classList.contains("invisible") && menuItems[14].classList.contains("invisible")) {
    for (let i = 0; i < 6; i++) {
      menuItems[i].classList.remove("invisible")
    }
    for (let i = 6; i < 12; i++) {
      menuItems[i].classList.add("invisible")
    }
    if (!menuItems[12].classList.contains("invisible")) {
      for (let i = 12; i < menuItems.length; i++) {
        menuItems[i].classList.add("invisible")
      }
    }
    if (btnArrowNext.classList.contains("invisible")) {
      btnArrowNext.classList.remove("invisible")
    }

    btnArrowPrev.classList.add("invisible")
  } else if (menuItems[8].classList.contains("invisible")) {
    for (let i = 6; i < 12; i++) {
      menuItems[i].classList.remove("invisible")
    }
    for (let i = 12; i < menuItems.length; i++) {
      menuItems[i].classList.add("invisible")
    }

    btnArrowNext.classList.remove("invisible")
  }
})
