const card = document.querySelectorAll(".news__card-wrapper")
const menuBtnNews = document.querySelectorAll(".btns-menu")

function paginate(card) {

  const allPage = card.length / 11

  const Pagination = {
    code: "",

    Extend: function(data) {
      data = data || {}
      Pagination.size = data.size || 11
      Pagination.page = data.page || 1
      Pagination.step = data.step || 3
    },

    Add: function(s, f) {
      for (let i = s; i < f; i++) {
        Pagination.code += "<a class=\"news__pagination-number\">" + i + "</a>"
      }
    },

    Last: function() {
      Pagination.code += "<i>...</i><a class=\"news__pagination-number\">" + Pagination.size + "</a>"
    },

    First: function() {
      Pagination.code += "<a class=\"news__pagination-number\">1</a><i>...</i>"
    },

    Click: function() {
      Pagination.page = +this.innerHTML
      Pagination.Start()
    },

    Prev: function() {
      Pagination.page--
      if (Pagination.page < 1) {
        Pagination.page = 1
      }
      Pagination.Start()
    },

    Next: function() {
      Pagination.page++
      if (Pagination.page > Pagination.size) {
        Pagination.page = Pagination.size
      }
      Pagination.Start()
    },

    Bind: function() {
      const a = Pagination.e.getElementsByTagName("a")
      for (let i = 0; i < a.length; i++) {
        if (+a[i].innerHTML === Pagination.page) a[i].className = "news__pagination-number current"
        a[i].addEventListener("click", Pagination.Click, false)
      }

      const itemsCard = document.querySelectorAll(".news__card-wrapper")
      itemsCard.forEach(item => {
        if (!item.classList.contains("invisible")) {
          item.classList.add("invisible")
        }
      })
      if (Pagination.page === 1) {
        for (let i = 0; i < 11; i++) {
          itemsCard[i].classList.remove("invisible")
        }
      } else {
        for (let i = Pagination.page * 12 - 13; i < Pagination.page * 12 - 1; i++) {
          itemsCard[i].classList.remove("invisible")
        }
      }
    },

    Finish: function() {
      Pagination.e.innerHTML = Pagination.code
      Pagination.code = ""
      Pagination.Bind()
    },

    Start: function() {
      if (Pagination.size < Pagination.step * 2 + 6) {
        Pagination.Add(1, Pagination.size + 1)
      } else if (Pagination.page < Pagination.step * 2 + 1) {
        Pagination.Add(1, Pagination.step * 2 + 4)
        Pagination.Last()
      } else if (Pagination.page > Pagination.size - Pagination.step * 2) {
        Pagination.First()
        Pagination.Add(Pagination.size - Pagination.step * 2 - 2, Pagination.size + 1)
      } else {
        Pagination.First()
        Pagination.Add(Pagination.page - Pagination.step, Pagination.page + Pagination.step + 1)
        Pagination.Last()
      }
      Pagination.Finish()
    },

    Buttons: function(e) {
      const nav = e.getElementsByTagName("a")
      nav[0].addEventListener("click", Pagination.Prev, false)
      nav[1].addEventListener("click", Pagination.Next, false)
    },

    Create: function(e) {
      const html = [
        "<a class=\"news__pagination-arrow-prev\"></a>", // previous button
        "<span class=\"news__pagination-wrapper\"></span>", // pagination container
        "<a class=\"news__pagination-arrow-next\"></a>", // next button
      ]

      e.innerHTML = html.join("")
      Pagination.e = e.getElementsByTagName("span")[0]
      Pagination.Buttons(e)
    },

    Init: function(e, data) {
      Pagination.Extend(data)
      Pagination.Create(e)
      Pagination.Start()
    },
  }

  const init = function() {
    Pagination.Init(document.getElementById("pagination"), {
      size: allPage, // pages size
      page: 1, // selected page
      step: 1, // pages before and after current
    })
  }

  init()
}

paginate(card);
menuBtnNews.forEach(function(tab, i) {
  tab.addEventListener("click", function() {
    if (document.querySelector(".btns-menu.active")) {
      document.querySelector(".btns-menu.active").classList.remove("active")
    }
    if (!document.querySelector(".news__search-info").classList.contains("invisible")) {
      document.querySelector(".news__search-info").classList.add("invisible")
    }
    if (!document.querySelector(".news__items-container-bg").classList.contains("invisible")) {
      document.querySelector(".news__items-container-bg").classList.add("invisible")
    }
    if (!document.querySelector(".news__search-no-result").classList.contains("invisible")) {
      document.querySelector(".news__search-no-result").classList.add("invisible")
    }
    if (!document.querySelector(".news-recommendations").classList.contains("invisible")) {
      document.querySelector(".news-recommendations").classList.add("invisible")
    }
    menuBtnNews[i].classList.toggle("active")
    document.getElementById("pagination").classList.add("invisible")
    if (!document.getElementById("pagination2").classList.contains("invisible")) {
      document.getElementById("pagination2").classList.add("invisible")
    }
   // const allItems = document.querySelectorAll(".news__card-wrapper")

    card.forEach(item => {
      if (!item.classList.contains("invisible")) {
        item.classList.add("invisible")
      }
    })

    const newArraw = document.querySelectorAll(`.news__card-wrapper[data-card = "${tab.dataset.btnsMenu.toLowerCase()}"]`)

    const Pagination = {
      code: "",

      Extend: function(data) {
        data = data || {}
        Pagination.size = data.size || 11
        Pagination.page = data.page || 1
        Pagination.step = data.step || 3
      },

      Add: function(s, f) {
        for (let i = s; i < f; i++) {
          Pagination.code += "<a class=\"news__pagination-number\">" + i + "</a>"
        }
      },

      Last: function() {
        Pagination.code += "<i>...</i><a class=\"news__pagination-number\">" + Pagination.size + "</a>"
      },

      First: function() {
        Pagination.code += "<a class=\"news__pagination-number\">1</a><i>...</i>"
      },

      Click: function() {
        Pagination.page = +this.innerHTML
        Pagination.Start()
      },

      Prev: function() {
        Pagination.page--
        if (Pagination.page < 1) {
          Pagination.page = 1
        }
        Pagination.Start()
      },

      Next: function() {
        Pagination.page++
        if (Pagination.page > Pagination.size) {
          Pagination.page = Pagination.size
        }
        Pagination.Start()
      },

      Bind: function() {
        const a = Pagination.e.getElementsByTagName("a")
        for (let i = 0; i < a.length; i++) {
          if (+a[i].innerHTML === Pagination.page) a[i].className = "news__pagination-number current"
          a[i].addEventListener("click", Pagination.Click, false)
        }
        if (menuBtnNews[i].getAttribute("data-btns-menu") === "all") {

          if (Pagination.page === 1) {
            for (let i = 0; i < 11; i++) {
              card[i].classList.remove("invisible")
            }
          } else {
            for (let i = Pagination.page * 12 - 13; i < Pagination.page * 12 - 1; i++) {
              card[i].classList.remove("invisible")
            }
          }
          document.getElementById("pagination").classList.remove("invisible")
        }
        if (newArraw.length > 12) {
          newArraw.forEach(item => {
            if (!item.classList.contains("invisible")) {
              item.classList.add("invisible")
            }
          })

          for (let j = Pagination.page * 12 - 12; j < Pagination.page * 12; j++) {
            newArraw[j].classList.remove("invisible")
          }
          document.getElementById("pagination2").classList.remove("invisible")
        } else {
          newArraw.forEach(i => {
            i.classList.remove("invisible")
          })
        }
      },

      Finish: function() {
        Pagination.e.innerHTML = Pagination.code
        Pagination.code = ""
        Pagination.Bind()
      },

      Start: function() {
        if (Pagination.size < Pagination.step * 2 + 6) {
          Pagination.Add(1, Pagination.size + 1)
        } else if (Pagination.page < Pagination.step * 2 + 1) {
          Pagination.Add(1, Pagination.step * 2 + 4)
          Pagination.Last()
        } else if (Pagination.page > Pagination.size - Pagination.step * 2) {
          Pagination.First()
          Pagination.Add(Pagination.size - Pagination.step * 2 - 2, Pagination.size + 1)
        } else {
          Pagination.First()
          Pagination.Add(Pagination.page - Pagination.step, Pagination.page + Pagination.step + 1)
          Pagination.Last()
        }
        Pagination.Finish()
      },

      Buttons: function(e) {
        const nav = e.getElementsByTagName("a")
        nav[0].addEventListener("click", Pagination.Prev, false)
        nav[1].addEventListener("click", Pagination.Next, false)
      },

      Create: function(e) {
        const html = [
          "<a class=\"news__pagination-arrow-prev\"></a>", // previous button
          "<span class=\"news__pagination-wrapper\"></span>", // pagination container
          "<a class=\"news__pagination-arrow-next\"></a>", // next button
        ]

        e.innerHTML = html.join("")
        Pagination.e = e.getElementsByTagName("span")[0]
        Pagination.Buttons(e)
      },

      Init: function(e, data) {
        Pagination.Extend(data)
        Pagination.Create(e)
        Pagination.Start()
      },
    }

    const init = function() {
      Pagination.Init(document.getElementById("pagination2"), {
        size: newArraw.length / 11, // pages size
        page: 1, // selected page
        step: 1, // pages before and after current
      })
    }

    init()
  })
})

const formSearch = document.querySelector(".news__menu-search-form")

const inputSearch = formSearch.querySelector(".search-input")
const newList = []
formSearch.addEventListener("submit", function(event) {
  event.preventDefault()
  if (document.querySelector(".btns-menu.active")) {
    document.querySelector(".btns-menu.active").classList.remove("active")
  }
  document.getElementById("pagination").classList.add("invisible")
  if (!document.getElementById("pagination2").classList.contains("invisible")) {
    document.getElementById("pagination2").classList.add("invisible")
  }
  if (!document.querySelector(".news__search-no-result").classList.contains("invisible")) {
    document.querySelector(".news__search-no-result").classList.add("invisible")
  }
  if (!document.querySelector(".news-recommendations").classList.contains("invisible")) {
    document.querySelector(".news-recommendations").classList.add("invisible")
  }
  if (document.querySelector(".news__info-wrapper").classList.contains("search-result")) {
    document.querySelector(".news__info-wrapper.search-result").classList.remove("search-result")
  }

  card.forEach(item => {
    if (!item.classList.contains("invisible")) {
      item.classList.add("invisible")
    }
  })

  if (inputSearch.value !== "") {
     newList.length = 0
    if (!document.querySelector(".news__search-info").classList.contains("invisible")) {
      document.querySelector(".news__search-info").classList.add("invisible")
    }
    if (!document.querySelector(".news__items-container-bg").classList.contains("invisible")) {
      document.querySelector(".news__items-container-bg").classList.add("invisible")
    }
    document.querySelectorAll(".news__search-word").forEach(el => {
      el.innerHTML = inputSearch.value
    })

    card.forEach(el => {

      if (el.querySelector(".news__card-title").innerHTML.toLowerCase().includes(inputSearch.value.toLowerCase())) {
        newList.push(el)
      }
    })
    document.querySelectorAll(".news__search-total").forEach(el => {
      el.innerHTML = newList.length
    })
    if (newList.length > 0) {
      document.querySelector(".news__search-info.invisible").classList.remove("invisible")
      document.querySelector(".news__items-container-bg.invisible").classList.remove("invisible")
      document.querySelector(".news__info-wrapper").classList.add("search-result")
    } else if (newList.length === 0) {
      document.querySelector(".news__search-no-result.invisible").classList.remove("invisible")
      document.querySelector(".news__text-content-btn-more").classList.add("invisible")

      document.querySelector(".news-recommendations.invisible").classList.remove("invisible")
}

    card.forEach(item => {
      if (!item.classList.contains("invisible")) {
        item.classList.add("invisible")
      }
    })

    const Pagination = {
      code: "",

      Extend: function(data) {
        data = data || {}
        Pagination.size = data.size || 11
        Pagination.page = data.page || 1
        Pagination.step = data.step || 3
      },

      Add: function(s, f) {
        for (let i = s; i < f; i++) {
          Pagination.code += "<a class=\"news__pagination-number\">" + i + "</a>"
        }
      },

      Last: function() {
        Pagination.code += "<i>...</i><a class=\"news__pagination-number\">" + Pagination.size + "</a>"
      },

      First: function() {
        Pagination.code += "<a class=\"news__pagination-number\">1</a><i>...</i>"
      },

      Click: function() {
        Pagination.page = +this.innerHTML
        Pagination.Start()
      },

      Prev: function() {
        Pagination.page--
        if (Pagination.page < 1) {
          Pagination.page = 1
        }
        Pagination.Start()
      },

      Next: function() {
        Pagination.page++
        if (Pagination.page > Pagination.size) {
          Pagination.page = Pagination.size
        }
        Pagination.Start()
      },

      Bind: function() {
        const a = Pagination.e.getElementsByTagName("a")
        for (let i = 0; i < a.length; i++) {
          if (+a[i].innerHTML === Pagination.page) a[i].className = "news__pagination-number current"
          a[i].addEventListener("click", Pagination.Click, false)
        }
        newList.forEach(item => {
          if (!item.classList.contains("invisible")) {
            item.classList.add("invisible")
          }
        })
        if (newList.length > 12) {
          for (let j = Pagination.page * 12 - 12; j < Pagination.page * 12; j++) {
            newList[j].classList.remove("invisible")
          }
          document.getElementById("pagination2").classList.remove("invisible")
          if (document.querySelector(".news__text-content-btn-more.invisible")) {
            document.querySelector(".news__text-content-btn-more.invisible").classList.remove("invisible")
          }
          document.querySelector(".news__search-visible").innerHTML = `1 - ${newList.filter(el => el.classList.value !== "news__card-wrapper invisible").length}`
        } else {
          newList.forEach(i => {
            i.classList.remove("invisible")
           // document.querySelector(".news__search-visible").innerHTML = `1 - ${newList.filter(el => el.classList.value !== "news__card-wrapper invisible").length}`
          })
          document.querySelector(".news__search-visible").innerHTML = `1 - ${newList.filter(el => el.classList.value !== "news__card-wrapper invisible").length}`
          document.querySelector(".news__text-content-btn-more").classList.add("invisible")
        }
       // document.querySelector(".news__search-visible").innerHTML = `1 - ${newList.filter(el => el.classList.value !== "news__card-wrapper invisible").length}`
      },

      Finish: function() {
        Pagination.e.innerHTML = Pagination.code
        Pagination.code = ""
        Pagination.Bind()
      },

      Start: function() {
        if (Pagination.size < Pagination.step * 2 + 6) {
          Pagination.Add(1, Pagination.size + 1)
        } else if (Pagination.page < Pagination.step * 2 + 1) {
          Pagination.Add(1, Pagination.step * 2 + 4)
          Pagination.Last()
        } else if (Pagination.page > Pagination.size - Pagination.step * 2) {
          Pagination.First()
          Pagination.Add(Pagination.size - Pagination.step * 2 - 2, Pagination.size + 1)
        } else {
          Pagination.First()
          Pagination.Add(Pagination.page - Pagination.step, Pagination.page + Pagination.step + 1)
          Pagination.Last()
        }
        Pagination.Finish()
      },

      Buttons: function(e) {
        const nav = e.getElementsByTagName("a")
        nav[0].addEventListener("click", Pagination.Prev, false)
        nav[1].addEventListener("click", Pagination.Next, false)
      },

      Create: function(e) {
        const html = [
          "<a class=\"news__pagination-arrow-prev\"></a>", // previous button
          "<span class=\"news__pagination-wrapper\"></span>", // pagination container
          "<a class=\"news__pagination-arrow-next\"></a>", // next button
        ]

        e.innerHTML = html.join("")
        Pagination.e = e.getElementsByTagName("span")[0]
        Pagination.Buttons(e)
      },

      Init: function(e, data) {
        Pagination.Extend(data)
        Pagination.Create(e)
        Pagination.Start()
      },
    }

    const init = function() {
      Pagination.Init(document.getElementById("pagination2"), {
        size: newList.length / 11, // pages size
        page: 1, // selected page
        step: 1, // pages before and after current
      })
    }


    init()
  }

  inputSearch.value = ""
})
const btnMoreNews = document.querySelector(".news__button-more")
btnMoreNews.addEventListener("click", () => {
  document.querySelector(".news__search-visible").innerHTML = `1 - ${newList.filter(el => el.classList.value !== "news__card-wrapper invisible").length}`
  if (document.querySelector(".swiper-slide-active").getAttribute("data-btns-menu") === "all" && newList.length === 0) {
    const nextNews = document.querySelectorAll(".news__card-wrapper.invisible ")
    if (nextNews.length !== 0) {
      for (let i = 0; i < 11; i++) {
        nextNews[i].classList.remove("invisible")
      }
    }
    if (nextNews.length <= 11) {
      document.querySelector(".news__text-content-btn-more").classList.add("invisible")
    }
  } else if (document.querySelector(".swiper-slide-active").getAttribute("data-btns-menu") !== "all" && newList.length === 0) {
    const dataAtr = document.querySelector(".swiper-slide-active").getAttribute("data-btns-menu")

    const newArrawMob = document.querySelectorAll(`.news__card-wrapper[data-card = "${dataAtr.toLowerCase()}"].invisible`)

    if (newArrawMob.length !== 0) {
      for (let i = 0; i < 11; i++) {
        newArrawMob[i].classList.remove("invisible")
      }
    }
    if (newArrawMob.length <= 11) {
      document.querySelector(".news__text-content-btn-more").classList.add("invisible")
    }
  } else {
    const newListMob = newList.filter(el => el.classList.value === "news__card-wrapper invisible")

    if (newListMob.length > 12) {
      for (let i = 0; i < 12; i++) {
        newListMob[i].classList.remove("invisible")
      }
      document.querySelector(".news__search-visible").innerHTML = `1 - ${newList.filter(el => el.classList.value === "news__card-wrapper").length}`
    } else if (newListMob.length <= 12) {
      for (let i = 0; i < newListMob.length; i++) {
        newListMob[i].classList.remove("invisible")
      }
      document.querySelector(".news__text-content-btn-more").classList.add("invisible")
      document.querySelector(".news__search-visible").innerHTML = `1 - ${newList.length}`
    }
  }

})
