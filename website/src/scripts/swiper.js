import Swiper from "swiper/bundle"

const swiper = new Swiper(".swiper-container", {
  interval: 5000,
  loop: true,
  spaceBetween: 0,
  effect: "fade",
  autoplay: true,

  navigation: {
    nextEl: ".swiper-btn-next",
    prevEl: ".swiper-btn-prev",
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  mousewheel: false,
  keyboard: true,
})

// eslint-disable-next-line no-unused-vars
const swiper2 = new Swiper(".swiper-container2", {
  cssMode: true,
  slidesPerView: 1,
  spaceBetween: 16,
  slidesPerGroup: 1,
  loop: true,
  loopFillGroupWithBlank: false,

  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  mousewheel: true,
  keyboard: true,

  breakpoints: {

    767: {
      slidesPerView: 2,
      spaceBetween: 30,
      slidesPerGroup: 2,
    },
    1025: {
      slidesPerView: 3,
      spaceBetween: 40,
      slidesPerGroup: 2,
    },
  },
})

// eslint-disable-next-line no-unused-vars
const swiper3 = new Swiper(".swiper-container3", {
  loop: true,
  spaceBetween: 0,
  // effect: "fade",
  effect: 'fade',
  fadeEffect: { // Add this
    crossFade: true,
  },
  speed: 2000,
  // autoplay: {
  //   delay: 4000,
  //   disableOnInteraction: false,
  // },
 autoHeight: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  mousewheel: false,
  keyboard: true,
})
const swiper4 = new Swiper(".news__swiper-container4", {
  loop: true,
  spaceBetween: 16,
  slidesPerView: "auto",
  slideToClickedSlide: true,
  swipe: true,
  mousewheel: false,
  keyboard: true,
});

swiper4.on('slideChange', function () {

  let curSlide = this.slides[ this.activeIndex ]
  const logClick = e => {

    const allItems = document.querySelectorAll(".news__card-wrapper")

    allItems.forEach(item => {
      if (!item.classList.contains("invisible")) {
        item.classList.add("invisible")
      }
    })
    if(document.querySelector(".news__info-wrapper").classList.contains("search-result")){
      document.querySelector(".news__info-wrapper.search-result").classList.remove("search-result")
    }
    if(!document.querySelector(".news__search-info").classList.contains("invisible")){
      document.querySelector(".news__search-info").classList.add("invisible")
    }
    if (!document.querySelector(".news-recommendations").classList.contains("invisible")) {
      document.querySelector(".news-recommendations").classList.add("invisible")
    }
    if (!document.querySelector(".news__search-no-result").classList.contains("invisible")){
      document.querySelector(".news__search-no-result").classList.add("invisible")
    }
    const newArrawMob = document.querySelectorAll(`.news__card-wrapper[data-card = "${e.target.dataset.btnsMenu.toLowerCase()}"]`)
    if (newArrawMob.length > 11 || e.target.dataset.btnsMenu === "all") {
      if(document.querySelector(".news__text-content-btn-more").classList.contains("invisible")){
        document.querySelector(".news__text-content-btn-more.invisible").classList.remove("invisible")
      }
      }else{
      document.querySelector(".news__text-content-btn-more").classList.add("invisible")
    }

    if (e.target.dataset.btnsMenu === "all") {
       for (let i = 0; i < 11; i++) {
        allItems[i].classList.remove("invisible")
      }

    }
    if (newArrawMob.length > 11) {
      newArrawMob.forEach(item => {
        if (!item.classList.contains("invisible")) {
          item.classList.add("invisible")
        }
      })

      for (let j = 0; j < 11; j++) {
        newArrawMob[j].classList.remove("invisible")
      }
    } else {
      newArrawMob.forEach(i => {
        i.classList.remove("invisible")
      })

    }
  }
  curSlide.addEventListener('click', logClick)

  curSlide.click()

 curSlide.removeEventListener('click', logClick)
});

const swiper5 = new Swiper(".swiper-container5", {
 cssMode: true,
  loop: true,
  slidesPerView: "auto",
  spaceBetween: 12,
  slidesPerGroup: 1,
  swipe: true,
  navigation: {
    nextEl: ".swiper-btn-next",
    prevEl: ".swiper-btn-prev",
  },

  mousewheel: true,
  keyboard: true,

})
const swiper6 = new Swiper(".swiper-container6", {
  cssMode: true,
  loop: true,
  slidesPerView: "auto",
  spaceBetween: 28,
  slidesPerGroup: 1,
  swipe: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },

  mousewheel: true,
  keyboard: true,

})
const swiper7 = new Swiper(".swiper-container7", {
  cssMode: true,
  loop: true,
  slidesPerView: "auto",
  spaceBetween: 28,
  slidesPerGroup: 1,
  swipe: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },

  mousewheel: true,
  keyboard: true,


  breakpoints: {
     1025: {
       slidesPerView: "auto",
       spaceBetween: 84,
       slidesPerGroup: 2,
    },
  },
})
