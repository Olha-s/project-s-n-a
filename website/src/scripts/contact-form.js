const tabsQuestion = document.querySelectorAll(".contact-section2__subtitle")
const answers = document.querySelectorAll(".contact-section2__text")

tabsQuestion.forEach(function (tab, i) {
  tab.addEventListener("click", function () {
    tabsQuestion[i].classList.toggle("active")
    answers[i].classList.toggle("open")
  })
})

const form = document.querySelector(".formWithValidation")
const select = form.querySelector(".select")

const email = form.querySelector(".email")

// form.addEventListener("submit", function (event) {
//   event.preventDefault()
//   //console.log('clicked on validate')
// })
const fields = form.querySelectorAll(".field")

form.addEventListener("submit", function (event) {
  event.preventDefault()

  removeValidation()

  checkFieldsPresence()

  checkMail()
})

function generateError(text) {
  const error = document.createElement("div")
  error.className = "error"
  error.style.color = "red"
  error.innerHTML = text
  return error
}

function removeValidation() {
  const errors = form.querySelectorAll(".error")

  for (let i = 0; i < errors.length; i++) {
    errors[i].remove()
  }
}

function checkFieldsPresence() {
  for (let i = 0; i < fields.length; i++) {
    if (!fields[i].value) {
      const error = generateError("Cant be blank")
      form[i].parentElement.insertBefore(error, fields[i])
    }
  }
}

function emailTest(input) {
  return !/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,8})+$/.test(input.value)
}

function checkMail() {
  if (email.value !== "" && emailTest(email)) {
    const error = generateError("Email is incorrect")
    email.parentElement.insertBefore(error, email)
  }
}

select.addEventListener("click", () => {
  document.querySelector(".contact-section3__select-arrow").classList.toggle("active")
  document.querySelector(".contact-section3__select-dropdown").classList.toggle("active")
})
document.querySelector(".contact-section3__select-arrow").addEventListener("click", () => {
  document.querySelector(".contact-section3__select-arrow").classList.toggle("active")
  document.querySelector(".contact-section3__select-dropdown").classList.toggle("active")
})

const items = document.querySelectorAll(".contact-section3__select-dropdown-item")
items.forEach(function (item, i) {
  item.addEventListener("click", function () {
    select.value = items[i].innerHTML
    document.querySelector(".contact-section3__select-arrow").classList.toggle("active")
    document.querySelector(".contact-section3__select-dropdown").classList.toggle("active")
  })
})
const btnViewAll = document.getElementById("contact-section2-btn")
btnViewAll.addEventListener("click", () => {
  document.querySelector(".contact-section2__text-content-question.invisible").classList.remove("invisible")
  document.querySelector(".contact-section2__text-content-btn").classList.toggle("invisible")
})
