const btnReadMore = document.querySelector(".read-more__button")
btnReadMore.addEventListener("click", () => {
  document.querySelector(".read-more__text-content").classList.toggle("text")
  document.querySelector(".read-more__paragraph.invisible").classList.remove("invisible")
  btnReadMore.classList.toggle("invisible")
})