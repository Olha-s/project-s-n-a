const tabs = document.querySelectorAll(".section6-tabs-title")
const contents = document.querySelectorAll(".tab-content")

tabs.forEach(function (tab, i) {
  tab.addEventListener("click", function () {
    document.querySelector(".tab-content.open").classList.remove("open")
    document.querySelector(".section6-tabs-title.active").classList.remove("active")
    tabs[i].classList.toggle("active")
    contents[i].classList.toggle("open")
  })
})

const smoothLinks = document.querySelectorAll('a[href^="#"]')
for (let smoothLink of smoothLinks) {
  smoothLink.addEventListener("click", function (e) {
    e.preventDefault()
    const id = smoothLink.getAttribute("href")
    document.querySelector(id).scrollIntoView({
      behavior: "smooth",
      block: "start",
    })
  })
}

const burger = document.querySelector(".header-burger-tablet")
const menuTablet = document.querySelector(".header-dropdown")
const headerTablet = document.querySelector(".header-wrapper-tablet")
burger.addEventListener("click", function (event) {
  event.stopPropagation()
  this.classList.toggle("active")
  menuTablet.classList.toggle("active")
  headerTablet.classList.toggle("active")
})

const btnSelect = document.querySelector(".select")
const selectMenu = document.querySelector(".select__items")
btnSelect.addEventListener("click", event => {
  event.stopPropagation()
  btnSelect.classList.toggle("active")
  selectMenu.classList.toggle("active")
})

const btnSelectTablet = document.querySelector(".select.tablet")
const selectMenuTablet = document.querySelector(".select__items.tablet")
btnSelectTablet.addEventListener("click", event => {
  event.stopPropagation()
  btnSelectTablet.classList.toggle("active")
  selectMenuTablet.classList.toggle("active")
})
document.addEventListener("click", function (event) {
  // const its_menu = event.target === selectMenu || selectMenu.contains(event.target);
  const its_btnSelect = event.target === btnSelect
  const menu_active = selectMenu.classList.contains("active")
  if (!its_btnSelect && menu_active) {
    btnSelect.classList.toggle("active")
    selectMenu.classList.toggle("active")
  }
})
